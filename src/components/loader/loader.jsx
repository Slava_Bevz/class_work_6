import React from "react";

const Loader = () => {
    return (
        <div class="spinner-grow" role="status">
            <span class="visually-hidden">Загрузка...</span>
        </div>
    )
}

export default Loader;