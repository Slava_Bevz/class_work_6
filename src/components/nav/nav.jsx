import React, {Component} from 'react';
import NavItem from '../nav-item/nav-item.jsx'
import request from '../../service/data.js'
import Loader from '../loader/loader.jsx'


import './nav.css';

class Nav extends Component {
    state = {
        data : null,
    }

    componentDidMount(){
        request.then((value) =>{
            this.setState((state) => {
                return {
                    data : value
                }
            })
        })
    }


    render(){
        console.log(this.state)
        return (
            <ul className='piple'>
                {
                   this.state.data === null? <Loader></Loader> : elements(this.state.data)
                }
            </ul>
        )
    }
}

function elements (a) {
    let btn = [];

    for (let e in a) {
        btn.push(<NavItem name={e}></NavItem>)
    }
    return btn
}

export default Nav;